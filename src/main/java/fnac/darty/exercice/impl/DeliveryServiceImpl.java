package fnac.darty.exercice.impl;

import fnac.darty.exercice.DeliveryService;
import fnac.darty.exercice.dto.Delivery;

import java.time.Clock;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

public class DeliveryServiceImpl implements DeliveryService {
    
    private final Clock clock;
    
    public DeliveryServiceImpl(Clock clock) {
        this.clock = clock;
    }
    
    public List<Delivery> processDeliveries(List<Delivery> deliveries) {
        
        deliveries.forEach(delivery -> delivery.deliver(LocalDate.now(clock)));

        deliveries.sort(Comparator.comparing(Delivery::getDeliveryDate));
        
        return deliveries;
    }
}
