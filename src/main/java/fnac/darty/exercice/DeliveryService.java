package fnac.darty.exercice;

import fnac.darty.exercice.dto.Delivery;

import java.util.List;

public interface DeliveryService {
    
    public List<Delivery> processDeliveries(List<Delivery> deliveries);
    
}
