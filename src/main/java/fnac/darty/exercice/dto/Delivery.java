package fnac.darty.exercice.dto;

import fnac.darty.exercice.enums.DeliveryStatus;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Delivery {
    
    private final String depart; 
    private final String arrivee; 
    
    private final Personne expediteur;
    private final Personne destinataire;
    
    private final LocalDate date;
    private DeliveryStatus deliveryStatus;

    public Delivery(String depart, String arrivee, Personne expediteur, Personne destinataire, LocalDate date, DeliveryStatus deliveryStatus) {
        this.depart = depart;
        this.arrivee = arrivee;
        this.expediteur = expediteur;
        this.destinataire = destinataire;
        this.date = date;
        this.deliveryStatus = deliveryStatus;
    }

    public boolean isValid(LocalDate deliveryDate) {
        
        return expediteur.isValid() 
                && destinataire.isValid() 
                && date != null 
                && !deliveryDate.isAfter(date.plus(2, ChronoUnit.MONTHS));
    }
    
    public boolean isDelayed(LocalDate deliveryDate) {
        return deliveryDate.isAfter(date) && deliveryDate.isBefore(date.plus(2, ChronoUnit.MONTHS));
    }
    
    public void deliver(LocalDate deliveryDate) {
        
        if(expediteur == null || destinataire == null) {
            return;
        }
        
        if(!isValid(deliveryDate)) {
            setInvalid();
            return;
        }
        
        if(isDelayed(deliveryDate)) {
            setDelayed();
            return;
        }
        
        setDelivered();
    }
    
    public LocalDate getDeliveryDate() {
        return date;
    }
    
    public DeliveryStatus getStatus() {
        return deliveryStatus;
    }
    
    private void setInvalid() { this.deliveryStatus = DeliveryStatus.INVALID; }
    
    private void setDelayed() {
        this.deliveryStatus = DeliveryStatus.DELAYED;
    }
    
    private void setDelivered() {
        this.deliveryStatus = DeliveryStatus.DELIVERED;
    }
    
}
