package fnac.darty.exercice.dto;

public class PersonnePhysique implements Personne {
    
    private final String nom;
    private final String prenom;
    private final long age;
    
    public PersonnePhysique(String nom, String prenom, long age) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
    }
    
    public boolean isValid() {
        return age >= 18;
    }
}
