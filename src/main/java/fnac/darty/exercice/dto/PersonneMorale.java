package fnac.darty.exercice.dto;

public class PersonneMorale implements Personne {

    private final String raisonSociale;
    private final long capital;
    
    public PersonneMorale(String raisonSociale, long capital) {
        this.raisonSociale = raisonSociale;
        this.capital = capital;
    }


    public boolean isValid() {
        return capital >= 5000;
    }
}
