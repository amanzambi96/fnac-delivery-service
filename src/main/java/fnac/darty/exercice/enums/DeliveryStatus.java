package fnac.darty.exercice.enums;

public enum DeliveryStatus {
    BEING_DELIVERED,
    DELAYED,
    DELIVERED,
    INVALID
}
