package fnac.darty.exercice;

import fnac.darty.exercice.dto.Delivery;
import fnac.darty.exercice.dto.Personne;
import fnac.darty.exercice.dto.PersonneMorale;
import fnac.darty.exercice.dto.PersonnePhysique;
import fnac.darty.exercice.enums.DeliveryStatus;
import fnac.darty.exercice.impl.DeliveryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class DeliveryServiceImplTest {
    
    private DeliveryService deliveryService;
    
    @BeforeEach
    void setUp() {
        this.deliveryService = 
                new DeliveryServiceImpl(Clock.fixed(Instant.parse("2022-03-22T00:00:00Z"), ZoneId.systemDefault()));
    }
    
    @ParameterizedTest
    @MethodSource("getDeliveries")
    void isDeliveryProcessed(Delivery input, DeliveryStatus expected) {
        List<Delivery> deliveries = Stream.of(input).collect(Collectors.toList());
        
        List<Delivery> processedDeliveries = deliveryService.processDeliveries(deliveries);
        Assertions.assertEquals(expected, processedDeliveries.get(0).getStatus());
    }
    
    @Test
    void should_return_deliveries_ordered_by_date() {
        // TODO: Complete
        Assertions.fail();
    }
    
    private static Stream<Arguments> getDeliveries() {
        return Stream.of(
          Arguments.of(delivery1(), DeliveryStatus.DELIVERED),
          Arguments.of(delivery2(), DeliveryStatus.INVALID),
          Arguments.of(delivery3(), DeliveryStatus.INVALID),
          Arguments.of(delivery4(), DeliveryStatus.DELAYED),
          Arguments.of(delivery5(), DeliveryStatus.INVALID),
          Arguments.of(delivery6(), DeliveryStatus.INVALID),
          Arguments.of(delivery7(), DeliveryStatus.BEING_DELIVERED),
          Arguments.of(delivery8(), DeliveryStatus.BEING_DELIVERED)
          );
    }
    
    
    private static Delivery delivery1() {
        return new Delivery(
                "106 Rue Richelieu, Paris",
                "21 Place des libertés, Bonneuil-sur-Marne",
                validPersonne1(),
                validPersonne2(),
                LocalDate.of(2022, 3, 22),
                DeliveryStatus.BEING_DELIVERED
        );
    }

    private static Delivery delivery2() {
        return new Delivery(
                "106 Rue Richelieu, Paris",
                "21 Place des libertés, Bonneuil-sur-Marne",
                invalidPersonne1(),
                validPersonne2(),
                LocalDate.of(2022, 3, 22),
                DeliveryStatus.BEING_DELIVERED
        );
    }

    private static Delivery delivery3() {
        return new Delivery(
                "106 Rue Richelieu, Paris",
                "21 Place des libertés, Bonneuil-sur-Marne",
                validPersonne1(),
                invalidPersonne1(),
                LocalDate.of(2022, 3, 22),
                DeliveryStatus.BEING_DELIVERED
        );    
    }

    private static Delivery delivery4() {
        return new Delivery(
                "106 Rue Richelieu, Paris",
                "21 Place des libertés, Bonneuil-sur-Marne",
                validPersonne1(),
                validPersonne2(),
                LocalDate.of(2022, 2, 22),
                DeliveryStatus.BEING_DELIVERED
        );
    }

    private static Delivery delivery5() {
        return new Delivery(
                "106 Rue Richelieu, Paris",
                "21 Place des libertés, Bonneuil-sur-Marne",
                validPersonne1(),
                validPersonne2(),
                LocalDate.of(2022, 1, 1),
                DeliveryStatus.BEING_DELIVERED
        );
    }

    private static Delivery delivery6() {
        return new Delivery(
                "106 Rue Richelieu, Paris",
                "21 Place des libertés, Bonneuil-sur-Marne",
                validPersonne1(),
                validPersonne2(),
                null,
                DeliveryStatus.BEING_DELIVERED
        );
    }

    private static Delivery delivery7() {
        return new Delivery(
                "106 Rue Richelieu, Paris",
                "21 Place des libertés, Bonneuil-sur-Marne", 
                null,
                validPersonne2(),
                LocalDate.of(2022, 3, 22),
                DeliveryStatus.BEING_DELIVERED
        );
    }

    private static Delivery delivery8() {
        return new Delivery(
                "106 Rue Richelieu, Paris",
                "21 Place des libertés, Bonneuil-sur-Marne",
                validPersonne1(),
                null,
                LocalDate.of(2022, 3, 22),
                DeliveryStatus.BEING_DELIVERED
        );
    }
    
    private static Personne validPersonne1() {
        return new PersonneMorale("EDF", 10000);
    }

    private static Personne validPersonne2() {
        return new PersonnePhysique("Doe", "John",  30);
    }
    
    private static Personne invalidPersonne1() {
        return new PersonneMorale("raison_sociale_test_2", 50);
    }

}