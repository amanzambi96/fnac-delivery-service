package fnac.darty.exercice;

import fnac.darty.exercice.dto.Personne;
import fnac.darty.exercice.dto.PersonnePhysique;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PersonnePhysiqueTest {
    
    @ParameterizedTest
    @MethodSource("getPersons")
    void isPersonValid(Personne input, boolean expected) {
        assertEquals(expected, input.isValid());
    }
    
    private static Stream<Arguments> getPersons() {
        return Stream.of(
                Arguments.of(personne1(), true),
                Arguments.of(personne2(), true),
                Arguments.of(personne3(), true),
                Arguments.of(personne4(), false),
                Arguments.of(personne5(), true)
        );
    }
        
    private static Personne personne1() {
        return new PersonnePhysique("Doe", "John", 30);
    }

    private static Personne personne2() {
        return new PersonnePhysique(null, "John", 30);
    }

    private static Personne personne3() {
        return new PersonnePhysique("Doe", null, 30);
    }
    
    private static Personne personne4() {
        return new PersonnePhysique("Doe", "John", 15);
    }

    private static Personne personne5() {
        return new PersonnePhysique("Doe", "John", 18);
    }
}