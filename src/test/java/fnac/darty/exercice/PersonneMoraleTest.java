package fnac.darty.exercice;

import fnac.darty.exercice.dto.Personne;
import fnac.darty.exercice.dto.PersonneMorale;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PersonneMoraleTest {

    @ParameterizedTest
    @MethodSource("getPersonnes")
    void isPersonneValid(Personne input, boolean expected) {
        assertEquals(expected, input.isValid());
    }
    
    private static Stream<Arguments> getPersonnes() {
        return Stream.of(
                Arguments.of(personne1(), true),
                Arguments.of(personne2(), true),
                Arguments.of(personne3(), false),
                Arguments.of(personne4(), true)
        );
    }
    
    private static Personne personne1() {
        return new PersonneMorale("raison_sociale_test", 5000);
    }

    private static Personne personne2() {
        return new PersonneMorale(null, 5000);
    }

    private static Personne personne3() {
        return new PersonneMorale("raison_sociale_test", 200);
    }

    private static Personne personne4() {
        return new PersonneMorale("raison_sociale_test", 10000);
    }
    
}