# FNAC - Delivery Service

Prérequis: Java 11

On veut pouvoir implementer un gestionnaire de livraisons

Voici quelques règles à prendre en compte :


- Une livraison se fait d'un endroit de départ vers une destination
- Une livraison est au nom d'un expediteur et d'un destinaire (personne morale ou physique)
- Une personne physique se définie avec un nom, prénom et age
- une personne morale, possède, quant à elle une raison sociale et un capital
- La livraison se fait à une date donnée
* si une date n'est pas fournie alors la livraison est jugée comme invalide
* si une date est passée (moins de deux mois) celle ci est marquée comme retardée sinon elle est invalide
- La livraison se fait si l'expediteur et le destinataire sont correctement renseigné
* dans le cas d'une personne physique la majorité est requise sinon la livraison est invalide
* dans le cas d'une personne morale le capital doit être supérieur à 5000 euros sinon la livraison est invalide
- On veut pouvoir gerer plusieurs livraisons en même temps
- Les livraisons doivent être triées de la plus ancienne à la plus récente

# TODO
- [ ] Compléter les tests du `DeliveryService`
- [ ] Compléter les tests de la classe `Delivery`
- [ ] Compléter la traduction vers l'anglais 
- [ ] Refactorer la méthode `deliveryService.processDeliveries()`: choisir si on veut muter la collection passée en paramètre ou non
- [ ] Tester les getters
- [ ] Add `.gitignore`